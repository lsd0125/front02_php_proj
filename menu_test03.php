<?php
require __DIR__.'/__connect_db.php';

// 順序必須是上層選單在前面
$sql = "SELECT * FROM `categories` ORDER BY `sid`";
$stmt = $pdo->query($sql);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

$a_data = [];

foreach($data as $v){
    $a_data[$v['sid']] = $v; // 設定時, array copy
}

foreach($a_data as $sid=>$v){
    if($v['parent_sid'] != 0){
        // 設定位址 (類似參照)
        //$a_data[$v['sid']]['parent'] = &$a_data[$v['parent_sid']];

        // $v (此項目)的上一層項目為: $a_data[$v['parent_sid']]
        // 此項目(目前的項目)的上一層，設定 children 元素，為目前的項目
        $a_data[$v['parent_sid']]['chldren'][$v['sid']] = &$a_data[$v['sid']];
    }
}

// print_r($a_data); // 可以看一下此時長什麼樣子
// exit;

// 拔掉不是第一層的
foreach($a_data as $v){
    if($v['parent_sid']!=0){
        unset($a_data[$v['sid']]);
    }
}

// print_r($a_data); // 可以看一下此時長什麼樣子
// exit;

$result =[
    'success'=>true,
    'data'=>$a_data,
];

echo json_encode($result, JSON_UNESCAPED_UNICODE);













